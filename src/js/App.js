import React from 'react';

const App = React.createClass({
  render() {
    return (
      <div className="root">
        <div className="no-protected text-xs-center">
          <form className="auth">
            <h2>Sign in</h2>
            <p className="lead">Enter your email address and password to sign in.</p>
            <p className="text-danger">
              <span>The account is not confirmed yet. </span>
              <a href="#">Send a confirmation email.</a>
            </p>
            <div className="form-group">
              <div className="false">
                <input type="email" className="form-control"  placeholder="Email" />
              </div>
            </div>
            <div className="form-group">
              <div className="input-with-icon password">
                <input type="password" className="form-control" placeholder="Password" />
                <img src="src/svg/auth/toggle-password.svg" alt=""/>
              </div>
            </div>
            <div className="form-group form-group-lg text-xs-right">
              <a href="#" className="warm-grey">Forgot Password?</a>
            </div>
            <div className="form-group form-group-lg">
              <button className="btn btn-primary btn-block">Sign In</button>
            </div>
            <p className="toggle-sign-view"><a href="#" className="bold">Sign up</a></p>
            <p className="lead link">
              <span>You can try our trial now! </span>
              <a href="#" className="warm-grey">Try demo</a>
            </p>
          </form>
        </div>
      </div>
    );
  }
});

export default App;
