var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var precss       = require('precss');

var config = {
    entry: {
        javascript: "./src/js/render.js",
        html: "./index.html",
    },
    resolve: { alias: {} },
    output: {
        path: './dist',
        filename: 'app.js',
    },
    module: {
        noParse: [],
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'react-hot!babel',
            },
            {
                test: /\.html$/,
                loader: 'file?name=[name].[ext]',
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader!postcss-loader'
            },
            {
                test   : /\.(png|jpg|jpeg|gif|ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
                loader : 'file-loader'
            },
        ],
        plugins: [
          new webpack.HotModuleReplacementPlugin()
        ]
    },
    postcss: function () {
        return [autoprefixer, precss];
    }
}
module.exports = config
